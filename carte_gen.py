#!/usr/bin/python

import os
import yaml

with open("carte.yml", "r") as f:
	carte = yaml.safe_load(f)

with open("carte.tex", "r") as f:
    latex_orig = f.read()

for (i,c) in enumerate(carte):
	latex = latex_orig
	latex = latex.replace("_t_", c["name"])
	for (j,d) in enumerate(c["content"]):
		latex = latex.replace(f"_d{j}_", d)
	with open(f"carte/carte_{i}.tex", "w") as f:
		f.write(latex)
	os.system(f"cd carte && latexmk carte_{i}.tex && latexmk -c && rm carte_{i}.tex")
