#!/bin/sh

latexmk camaleonte.tex
latexmk mappa.tex
latexmk -c

python carte_gen.py

pdfjam camaleonte.pdf mappa.pdf mappa.pdf mappa.pdf mappa.pdf mappa.pdf mappa.pdf mappa.pdf --nup 2x4 --paper a4paper --outfile printme_0.pdf
pdfjam carte/* --nup 2x2 --landscape --paper a4paper --outfile printme_1.pdf

pdftk printme_0.pdf printme_0.pdf printme_1.pdf cat output printme.pdf

rm printme_0.pdf printme_1.pdf
